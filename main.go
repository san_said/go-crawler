package main

import (
	"fmt"
	"log"
	"net/http"
	"regexp"
	"strings"

	gq "github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
)

type company struct {
	size     string
	name     string
	industry string
	// inSizeRange bool
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func appendStringSet(set map[string]struct{}, str string) {
	exists := struct{}{}

	if _, ok := set[str]; !ok {
		set[str] = exists
	}
}

func convertSetToArray(set map[string]struct{}) []string {
	setArray := []string{}

	for k, _ := range set {
		setArray = append(setArray, k)
	}

	return setArray
}

// TODO: Change to getLinks function which will scan for links matching a pattern
func scanHref(tokenizer *html.Tokenizer, pattern string) []string {
	paths := make(map[string]struct{})

	for {
		next := tokenizer.Next()

		switch {
		case next == html.StartTagToken:
			token := tokenizer.Token()

			isAnchor := token.Data == "a"

			if isAnchor {
				for _, a := range token.Attr {
					if a.Key == "href" {
						hrefMatchesPattern, err := regexp.MatchString(pattern, a.Val)

						fatal(err)

						if hrefMatchesPattern {
							appendStringSet(paths, a.Val)
							break
						}
					}
				}
			}
		case next == html.ErrorToken:
			return convertSetToArray(paths)
		}
	}
}

// func inSizeRange(sizeRange string, desiredRange) int {

// }

func getCompanyDetails(companyPaths []string) []company {
	companies := []company{}

	for _, path := range companyPaths {
		newCompany := company{}

		resp, err := http.Get("https://stackoverflow.com/" + path)
		defer resp.Body.Close()

		// respBody, err := ioutil.ReadAll(resp.Body)
		// fmt.Printf(string(respBody))

		doc, err := gq.NewDocumentFromResponse(resp)

		fatal(err)

		aboutNode := doc.Find(".fw-bold.fs-caption.fs-category.fc-black-400.mb0")
		nameNode := doc.Find(".fs-display1.lh-xs.fw-normal.mb8.md\\:pt16.fc-black-900")

		newCompany.name = nameNode.Text()

		industryNode := aboutNode.FilterFunction(func(num int, selection *gq.Selection) bool {
			return selection.Text() == "Industry"
		})
		newCompany.industry = strings.Trim(industryNode.Siblings().Text(), " ")

		sizeNode := aboutNode.FilterFunction(func(num int, selection *gq.Selection) bool {
			return selection.Text() == "Size"
		})
		newCompany.size = strings.Trim(sizeNode.Siblings().Text(), " ")

		companies = append(companies, newCompany)
	}

	return companies
}

func main() {
	mainSoPage, err := http.Get("https://stackoverflow.com/jobs/companies?l=United+Kingdom&d=20&u=Miles")
	defer mainSoPage.Body.Close()

	// bod, err := ioutil.ReadAll(mainSoPage.Body)
	// fmt.Printf(string(bod))

	fatal(err)

	tokenizer := html.NewTokenizer(mainSoPage.Body)

	companyPaths := scanHref(tokenizer, `^/jobs/companies/.+Miles$`)
	companies := getCompanyDetails(companyPaths)

	for _, comp := range companies {
		fmt.Printf("%+v", comp)
	}

	// for _, path := range companyPaths {
	// 	resp, err := http.Get("https://stackoverflow.com/" + path)
	// 	defer resp.Body.Close()

	// 	bodyBytes, err := ioutil.ReadAll(resp.Body)

	// 	if err == nil {
	// 		fmt.Printf(string(bodyBytes))
	// 	}
	// }

	// fmt.Printf("%+v", resultMap)
}

/*
TODO:
[ ] Update scanHref to use goquery
[ ] Find a way to remove all white space from the string text
[ ] Create a way to search based on company size
[ ] Define an output type for results depending on CRM
[ ] Iterate through all pages of StackOverflow
[x] Iterate through all paths found for companies
[x] Fix goquery functionality
*/
