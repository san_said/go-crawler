package main

import (
	"reflect"
	"testing"
)

func TestConvertSetToArray(t *testing.T) {
	type testScenario struct {
		input  map[string]struct{}
		result []string
	}

	testTable := []testScenario{
		{
			map[string]struct{}{},
			[]string{}},
		{
			map[string]struct{}{
				"ABC": struct{}{},
			},
			[]string{"ABC"},
		},
		{
			map[string]struct{}{
				"DEF": struct{}{},
				"GHI": struct{}{},
			},
			[]string{"DEF", "GHI"},
		},
	}

	for _, scenario := range testTable {
		Check(convertSetToArray(scenario.input), scenario.result, t)
	}
}

func Check(input interface{}, expected interface{}, t *testing.T) {
	if !(reflect.DeepEqual(input, expected)) {
		t.Error("Expected:", expected, "Got:", input)
	} else {
		t.Log("SUCCESS")
	}
}
